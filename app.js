let toaddElement=document.getElementById('to-do-container');
let toaddElement1=document.getElementById('Active');
let toaddElement2=document.getElementById('Completed');
let anchorElement1=document.getElementById("All");
let anchorElement2=document.getElementById("Active1");
let anchorElement3=document.getElementById("Completed2");

anchorElement1.onclick = function () {
    toaddElement.style.display="block"
    toaddElement1.style.display = "none";
    toaddElement2.style.display = "none"

};

anchorElement2.onclick = function () {
    toaddElement.style.display="none"
    toaddElement1.style.display = "block";
    toaddElement2.style.display = "none"
};

anchorElement3.onclick = function () {
    toaddElement.style.display="none"
    toaddElement1.style.display = "none";
    toaddElement2.style.display = "block"
};


let count=0;

function createToDo(){
    count=count+1
    let todoID = 'todo'+ count
    let checkBoxId = 'checkbox'+count
    let labelId = 'label'+count 
    if(!document.getElementById('inputcontainer').value){
        alert("Please provide a input")
        return;
    }
    
    //container element
    let divElement=document.createElement("div");
    divElement.className="createdContainer";
    divElement.id = 'todo'+count

    // input element
    var checkbox = document.createElement('input');
    checkbox.type = "checkbox";
    checkbox.classList.add('checkbox')
    checkbox.id = checkBoxId;


    // label element
    var label = document.createElement('label')
    label.htmlFor = checkBoxId;
    label.id = labelId
    label.innerHTML=document.getElementById('inputcontainer').value;
    label.classList.add('labelElement')
    document.getElementById('inputcontainer').value="";
    
    // append to created div
    divElement.appendChild(checkbox);
    divElement.appendChild(label);

    // appending to html container element
    toaddElement.appendChild(divElement);
    
    checkbox.addEventListener('click',function(){
        label.classList.toggle('text')
        toaddElement2.appendChild(div);
    })

}